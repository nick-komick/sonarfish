#' @title Plot Daily Estimate
#' @description Plot and save daily summary
#' @param daily_estimate output from daily_est()
#' @param output_filename name of output folder.  Default is "Processed_Sonar"
#' @param project_name specify project name
#' @importFrom dplyr "%>%"
#' @details Creates an output folder (default name is "Processed_Sonar") inside
#'   the working directory and writes a .pdf file containing daily passage plot
#'   into a subfolder named "Plots".
#' @export
daily_estimate_plot <-
  function(daily_estimate,
           output_filename = "Processed_Sonar",
           project_name) {
    #plot daily passage

    if (ncol(daily_estimate) == 3) {
      a <<-
        ggplot2::ggplot(daily_estimate, ggplot2::aes(x = Date, y = daily_estimate[, 2])) +
        ggplot2::geom_line(size = c(1)) + ggplot2::labs(y = "Daily Passage Estimate", x =
                                                          "Date") + ggplot2::theme_classic()



      } else  {
        a <-
          ggplot2::ggplot(daily_estimate, ggplot2::aes(x = Date, y = daily_estimate[, 4])) +
          ggplot2::geom_line(size = c(1)) + ggplot2::labs(y = "Daily Passage Estimate", x =
                                                            "Date") + ggplot2::theme_classic()
    }


    #Create Output directory to copy files to
    FolderName <-
      paste(getwd(), output_filename, "Plots/", sep = "/")
    dir.create(FolderName, recursive = TRUE)# Make sure the directory exists

    pathout <- paste(FolderName)
    FileName <-
      paste(pathout,
            project_name,
            "_",
            "Daily_Passage",
            ".pdf" ,
            sep = "")  ###specifies order of file name
   a
   ggplot2::ggsave(FileName)


  }
