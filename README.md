
# sonarfish

<!-- badges: start -->
<!-- badges: end -->

Use to compile marked sonar files (marked with Echotastic 3) into a single folder, enter raw mark data into a single database, estimate and export hourly passage after adjusting for short files, estimate and export passage when data is missing using time-series algorith from imputeTS(), and calculate and export daily passage. Constructed for sonar enumeration programs utilizing one or more sonars, shooting at one or more scan ranges (i,e, Inshore, Offshore). Additionally calculates variance, average percent error, and confidence intervals around counts. 

## Installation

You can install sonarfish from [GitLab](https://CRAN.R-project.org) with:

``` r
install.packages("remotes")
remotes::install_git("https://gitlab.com/YR_Watershed/sonarfish.git")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(sonarfish)
## basic example code
```

