## ---- include = FALSE---------------------------------------------------------
knitr::opts_chunk$set(echo=TRUE,collapse = TRUE,  comment = "#>")
library("roxygen2")

## ----setup--------------------------------------------------------------------
library("sonarfish")
path <- paste0(rprojroot::find_package_root_file(), "/inst/extdata")
knitr:::opts_knit$set(root.dir = path) 

## ----simul8, warning=FALSE, eval=FALSE----------------------------------------
#  
#  #Identify location of marked aris files (hardrives)
#  LB_files <-  ("Y:/3 - STOCK ASSESSMENT/Porcupine River Sonar/Data/Years/2019/Processed Sonar/all_txt_files/LB")
#  
#  #copy LB files into new subfolder in workign directory
#  copypaste_files(LB_files, output_subname = "ARIS_LB")
#  

## ----simulate_data2a, echo=FALSE----------------------------------------------

a <- read.table(paste0(getwd(),"/Processed_Sonar/ARIS_LB/Porcupine_LB_2019-07-01_170939.aris.txt"), header=FALSE, sep=",")

a

## ----simulate_data2, warning=FALSE--------------------------------------------
#identify new location of LB files
LB_files_new <- file.path(getwd(), "Processed_Sonar", "ARIS_LB")

#consolidate marked files into single database
consol_dat_LB <- consolidate_files(LB_files_new)

## ----simulate_data2b, warning=FALSE, message=FALSE----------------------------
head(consol_dat_LB)


## ----simulate_data3, message=FALSE, warning=FALSE-----------------------------
mark_dat_LB <- tidy_markdata(consol_dat_LB)

head(mark_dat_LB)

## ----simulate_data4, results='hide', warning= FALSE---------------------------
#specify which bank ("LB" or "RB") and range(s)
Porc_LB_br <- tidy_markdata_br(mark_dat_LB, ranges=2, bank="LB")

#save data
tidy_markdata_save(markdata=Porc_LB_br, bank="LB", project_name = "Porcupine.Sonar")


## ----data4--------------------------------------------------------------------
head(Porc_LB_br)

## ----simulate_data5, results='hide', message=FALSE, warning=FALSE-------------
#estimate hourly passage
Porc_LB_hr <- hr_passage(Porc_LB_br)


## ----dat5---------------------------------------------------------------------
head(Porc_LB_hr)

## ----RB_hidden, include=FALSE, warning=FALSE----------------------------------

#identify location of RB folders
RB_files_new <- file.path(getwd(), "Processed_Sonar", "ARIS_RB")

#consolidate marked files into single database
consol_dat_RB <- consolidate_files(RB_files_new)

#clean up and save markdata.
mark_dat_RB <- tidy_markdata(consol_dat_RB)
tidy_markdata_save(mark_dat_RB,bank="RB",project_name = "Porcupine.Sonar")

#specify bank ("LB" or "RB") and range(s)
Porc_RB_br <- tidy_markdata_br(mark_dat_RB, ranges=2, bank="RB")

#estimate hourly
Porc_RB_hr <- hr_passage(Porc_RB_br)


## ----merge LB/RB--------------------------------------------------------------
#if applicable, repeat above steps for the second bank (not shown)
#merge left and right bank data

df <- rbind(Porc_LB_hr, Porc_RB_hr)
tail(df)

## ----merge LB/RB2, warning=FALSE, results='hide',include=TRUE, message=FALSE----
#Passage using kalman imputation
df2 <- impute_kalman(df)

#Passage using linear imputation
df3 <- impute_linear(df)

#print missing stats
missing_stats(df2)


## ----LB/RB3,warning=FALSE, results='hide'-------------------------------------
passage_estimate_save(df2, project_name = "Porcupine.Sonar")

## ----pass, warning=FALSE, results='hide'--------------------------------------
#summarise daily passage
daily <- daily_est(df2)

#save daily estimate
daily_estimate_save(daily, project_name = "Porcupine.Sonar")

#save daily estimate plot
daily_estimate_plot(daily, project_name = "Porcupine.Sonar")


## ----ape, warning=FALSE, results='hide', echo=FALSE, message=FALSE------------
libs <- list( "dplyr", "lubridate")
lapply(libs, library, character.only = TRUE)

df <-
  read.csv(paste0(getwd(), "/Sonar.Recount.Log.csv"),
    header = TRUE
  )

#format data
df$Min <- minute(hm(df$Hour.File))
df <-
  df %>%  mutate(Shore = ifelse (Min == 30, "Offshore", "Inshore")) %>%  # Make a collum with Shore (based on Minure of the file),
  mutate(Species = ifelse(Date <= ymd("19/8/17"), "CH", "CM")) # Make collumn for species based on corssover date

#Make a vector of data with five collumns (three for counts, then bank, then shore) for function
counts <- select(df, Count, Count.1, Count.2, Bank, Shore, Species)
counts <- type.convert(counts) #clean up data types



## ----ape2 ,  warning=FALSE, message='hide'------------------------------------
head(counts)

#run function
APE <- sonar_APE(counts)


## ----var----------------------------------------------------------------------
#User must add a collumn specifying the species being assessed.  Use this format "Species"
df2$Species <- "CH" # add collumn for Chinook
VAR <- sonar_VAR2(df2)

## ----CI, message=FALSE, warning=FALSE-----------------------------------------
sonar_CI(APE = APE, VAR = VAR)

